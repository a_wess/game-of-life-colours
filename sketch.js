//Authour: Andrew Wess 2018,2020,2021

function setup() 
{
    createCanvas(1360, 768);
    frameRate(20);

  //Intentionally slowed down to allow the user to maintain the game of life population whilst moving towards the enter buttons

//The arrays containing the values of each cell. One for R,G and B repectively.
//Array of structure: [X,Y,alive]
livePieces = [];
livePieces2 = [];
livePieces3 = [];
for(i = 0; i < 10; i++)
{
    for(j = 0; j < 10; j++)
    {
        livePieces.push([i * 10, j * 10, false]);
        livePieces2.push([200 + (i * 10),  j * 10, false]);
	      livePieces3.push([ 400 + (i * 10),  (j * 10), false]);
    }
}
  //initialized the 3 boards


livePiecesCopy = livePieces.slice();
livePiecesCopy2 = livePieces2.slice();
livePiecesCopy3 = livePieces3.slice();
//copys of the boards
}


//function to decide whether a cell should be alive or dead
function gameOfLife(live)
{ 

    livesCopy = live.slice();
    for (i = 0; i < live.length; i++)
    {
        count = countNeighbours(live,i); 
   

        if (count < 2 || count > 3)
        {
            livesCopy[i][2] = false;
        }
        else
          {
            livesCopy[i][2] = true;
          }


    }

    return livesCopy;  
}


r = 0;
g = 0; 
b = 0;



function draw() 

{
  colour1 = r * 3;
  colour2 = g * 3;
  colour3 = b * 3;
  //each colour is multiplied by three to allow he user to select any rgb value

  if(colour1 < 0)
    colour1 = 0;
  else
  {
    if (colour1 > 255)
      colour1 = 255
  }

  if(colour2 < 0)
    colour2 = 0;
  else
  {
    if (colour2 > 255)
      colour2 = 255
  }

  if(colour3 < 0)
    colour3 = 0;
  else
  {
    if (colour3 > 255)
      colour3 = 255
  }

  //prevents the RGB values from being out of bounds
    
    population = 0;
   population2 = 0;
   population3 = 0;
    background(colour1,colour2,colour3);
    fill(255,255,255);
  strokeWeight(5);
  stroke(0);
  rect(width / 4,height / 4,width / 2,height / 2);
  strokeWeight(1);
  for (i = 0; i < livePieces.length; i++)  
        {  
          stroke(255);
          fill(255,0,0);
          rect(livePieces[i][0] + (width * 5 / 16),livePieces[i][1] + (height * 3 / 8),10,10);
          fill(0,255,0);
          rect(livePieces2[i][0] + (width * 5 / 16),livePieces2[i][1] + (height * 3 / 8),10,10);
          fill(0,0,255);
          rect(livePieces3[i][0] + (width * 5 / 16),livePieces3[i][1] + (height * 3 / 8),10,10);   
        }
        //draws the initial boards
  fill(0,0,0);

    stroke(205);
    fill(0);
    textSize(40);
    text("please select a colour", (width * 6 / 16), (height * 5 / 16));

     for (i = 0; i < livePieces.length; i++)
     { 

      neighbours = countNeighbours(livePieces,i);
      neighbours2 = countNeighbours(livePieces2,i);
      neighbours3 = countNeighbours(livePieces3,i);
      
        //calculates population, determines state of pieces and draws board 1
 
      drawOnBoard(livePieces,livePiecesCopy, neighbours, i, 0,population);
      drawOnBoard(livePieces2,livePiecesCopy2, neighbours2, i, (width * 10 / 32),population2);
      drawOnBoard(livePieces3,livePiecesCopy3,neighbours3, i, (width * (10 / 32)),population3);
      
     }
   
   mousexpos = Math.ceil(((mouseX - (width * 10 / 32)) / 10)) - 1;
   mouseypos = Math.ceil((mouseY - (height * 3 / 8)) / 10) - 1 ;

   //calculates the index number of a piece by calcuating the mouse's X and Y values on the board

   if (mouseIsPressed && mousexpos < 10 && mousexpos >= 0 && mouseypos < 10 && mouseypos >= 0)
       
       createNew(livePiecesCopy,0);

      
      //sets clicked piece and surrounding pieces when clicked for board 1
      if (mouseIsPressed && mousexpos < 33 && mousexpos >= 24 && mouseypos >= 0 && mouseypos < 10 )
        createNew(livePiecesCopy2,240);
      
      //sets clicked piece and surrounding pieces when clicked for board 2

      if (mouseIsPressed && mousexpos >= 48 && mousexpos <= 57 && mouseypos >= 0 && mouseypos < 10)
	createNew(livePiecesCopy3,480);
       
      //sets clicked piece and surrounding pieces when clicked  for board 3
  for (i = 0; i < livePieces.length; i++)
    {
      if(livePieces[i][2]) 
        population++;
      if(livePieces2[i][2])
        population2++;
      if(livePieces3[i][2])
        population3++;
    }
  if(mouseIsPressed && mouseX >= (width * 10 / 32) && mouseX <= ((width * 10 / 32) + (width / 16)) && mouseY >= (height * 5 / 8) && mouseY <= ((height / 16) + (height * 5 / 8)))  
    {
      r = population;
      console.log("pressedR", r);
    }
  
  

  //sets the 'r' value to the current population of the red game

  if(mouseIsPressed && mouseX >= (width * 15 / 32) && mouseX <= ((width * 15 / 32) + (width / 16)) && mouseY >= (height * 5 / 8) && mouseY <= ((height * 5 / 8) + (height / 16))) {
    g = population2;
    console.log("pressedG");
  }
  
  //sets the 'g' value to the current population of the green game

  if(mouseIsPressed && mouseX >= (width * 20 / 32) && mouseX <= ((width * 20 / 32) + (width * 16)) && mouseY >= (height * 5 / 8) && mouseY <= ((height * 5 / 8 ) + (height / 16))) {
    b = population3;
    console.log("pressedB");
  }
  
  
  //sets the 'b' value to the current population of the blue game

  fill(205,205,205);
  rect(width * 10 / 32 ,height * 5 / 8,width / 16,height / 16);
  rect(width  *  15 / 32,height * 5 / 8,width / 16,height / 16);
  rect(width * 20 / 32,height * 5 / 8,width / 16,height / 16);

  textSize(20);
  stroke (0);
  fill(0);
  
  
  text("r: " + population, (width * 10 / 32), (height * 9 / 16));
  text("g: " + population2, (width * 15 / 32), (height * 9 / 16));
  text("b: " + population3, (width * 20 / 32), (height * 9 / 16));
  //gives current population of each game to the user
  
  text("enter", width * 21 / 64, height * 42.5/64);
  text("enter", width * 31 / 64, height * 42.5/64);
  text("enter", width * 41 / 64, height * 42.5/64);

  gameOfLife(livePieces);
  gameOfLife(livePieces2);
  gameOfLife(livePieces3);

//labels for buttons

}

//functions to calculate the number of living neighbours for a given cell
function countNeighbours(livePieces, i)
{
      neighbours = 0;

      if ( (i % 10 != 0) && livePieces[i - 1][2] )
       {
       neighbours += 1;
	   }
  
      if (((i+1) % 10 != 0)  && livePieces[i + 1][2])
       {
       neighbours += 1;
	   }
  
      if (i < 90 && livePieces[i + 10][2])
       {
       neighbours += 1;
	   }
      if (((i+1) % 10 != 0) && i < 90 && livePieces[i + 11][2])
          {
       neighbours += 1;
	   }
      if (i > 9 && livePieces[i - 10][2])
          {
       neighbours += 1;
	   }
      if(i < 90 && ((i+1) % 10 != 0) && livePieces[i + 9][2])
          {
       neighbours += 1;
	   }
      if (i > 9 && (i % 10 != 0) && livePieces[i - 11][2])
          {
       neighbours += 1;
	   }
      if (i > 9 && ((i+1) % 10 != 0) && livePieces[i -9][2])
	{
       neighbours += 1;
	}
      return neighbours;
}

function drawOnBoard(livePieces, livePiecesCopy, neighbours, i, X,population)
{
        if (livePieces[i][2])  
        {
          population++;
          fill(0,0,0);
          rect(livePieces[i][0] + (width * 10 / 32),livePieces[i][1] + (height * 3 / 8),10,10);   
        }

      
}

//sets a cell and some surrounding cells to alive upon a mouse click
function createNew(livePiecesCopy, X)
{
	number = parseInt(String(mousexpos) + String(mouseypos)) - X;
    console.log("Number", number)
        if(number < 90)
        {
          livePiecesCopy[number + 9][2] = true;
        }
        if(((i+1) % 10 != 0) && number != 99)
        {
          livePiecesCopy[number + 1][2] = true;
        }
        if((i % 10 != 0) && number != 0)
        {
          livePiecesCopy[number - 1][2] = true;
        }
        if(number >= 9)
        {
          livePiecesCopy[number - 10][2] = true;
        }

        livePiecesCopy[number][2] = true;
}
